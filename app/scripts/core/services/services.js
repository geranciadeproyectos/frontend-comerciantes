'use strict';
comerciantesApp = angular.module('bcscApp');

comerciantesApp.service('tokenServices', [
  '$resource',
  'APP_CONFIG',
  function ($resource, config) {
    var url = config().path.api.token + '/:id';
    var paramDefault = {
      id: '@id'
    };

    var actions = {
      "update": {
        method: 'PUT',
        params: paramDefault
      }
    };

    return $resource(url, paramDefault, actions);
  }
]);

comerciantesApp.service('tempStorageData', [
  function () {
    return ({
      data: {}
    });
  }
]);

comerciantesApp.service('loginService', ['$http', '$q', '$resource', '$rootScope', 'APP_CONFIG',
  function ($http, $q, $resource, $rootScope, config) {
    return new function loginService() {
      this.login = function login(credentials, success, error) {
        var deffered = $q.defer();

        $http({
          method: 'POST',
          url: config().path.login,
          //$.param()
          params: {
            client_id: config().clientId,
            scope: 'read write',
            grant_type: 'password',
            username: credentials.username,
            password: credentials.password,
            client_secret: config().clientSecret
          },
          data: {
            username: credentials.username,
            password: credentials.password
          },
          headers: {
            //Authorization : config().clientId+':'+config().clientSecret
            //Authorization : 'Basic R0VPQ0VOVFJJWC1HRU9DT0RJTkctQVBQOjEyMzQ1Ng=='
            Authorization: config().clientAuth
          }
        }).then(function (response) {
            // success
            success(response);
          },
          function (response) { // optional
            // failed
            error(response);
          });
      };
      this.refreshLogin = function changePassword(success, error) {
        $http({
          method: 'POST',
          url: config().path.login,
          //$.param()
          params: {
            client_id: config().clientId,
            grant_type: 'refresh_token',
            refresh_token: $rootScope.refreshToken,
            client_secret: config().clientSecret
          },
          data: {
            refresh_token: $rootScope.refreshToken
          },
          headers: {
            //Authorization : config().clientId+':'+config().clientSecret
            //Authorization : 'Basic R0VPQ0VOVFJJWC1HRU9DT0RJTkctQVBQOjEyMzQ1Ng=='
            Authorization: config().clientAuth
          }
        }).then(function (response) {
            // success
            success(response);
          },
          function (response) { // optional
            // failed
            error(response);
          });
      };
      this.getUsersDetail = function logOut(success, error) {
        //success();
        $http({
          method: 'GET',
          url: config().path.user_details,
        }).then(function (response) {
            $http({
              method: 'GET',
              url: 'https://randomuser.me/api/?seed=' + response.data.usuario.nombre + '&inc=picture'
            })
              .success(function (data) {
                response.data.picture = data.results[0].picture.medium;
                success(response);
              }).error(function(error){
                response.data.picture = 'images/default.gif';
                success(response);
            });
          },
          function (response) { // optional
            // failed
            error(response);
          });
      };
      this.logOut = function logOut(success, error) {
        if (getCookie("access_token") != '') {
          $rootScope.accessToken = null;
          document.cookie = "access_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
        }
        if (getCookie("refresh_token") != '') {
          $rootScope.refreshToken = null;
          document.cookie = "refresh_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";

        }
        if (getCookie("token_expires") != '') {
          $rootScope.tokenExpires = null;
          document.cookie = "token_expires=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
        }
        if ($rootScope.authenticatedUser !== null || $rootScope.authenticatedUser !== undefined) {
          $rootScope.authenticatedUser = null;
        }
        success();
      };
      this.changePassword = function changePassword(data, success, error) {
        $http({
          method: 'PUT',
          url: config().path.changePassword,
          params: data,
        }).then(function successCallback(response) {
          success(response);
        }, function errorCallback(response) {
          error(response);
        });
      };
    };

    function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
      }
      return "";
    }
  }

]);

comerciantesApp.service('refrescarTokenUser', ['$rootScope', 'loginService',
  function ($rootScope, loginService) {
    return ({
      launch_sync: setSyncRefreshToken
    });

    var inProgress = false;

    function setSyncRefreshToken() {
      var dt_expires = new Date(Date.parse($rootScope.tokenExpires));
      var dt_now = new Date();
      var diff = dt_expires - dt_now;
      var lapse = diff - 10000;
      //var lapse = 3000;

      var _time = 10 - 6;
      setTimeout(function () {
        console.log('new Now: ' + new Date());
        if ($rootScope.refreshToken && $rootScope.refreshToken !== null) {
          console.log('runRefreshRoutine');
          runRefreshRoutine();
        }
      }, lapse);
    };

    function runRefreshRoutine() {
      if (inProgress) {
        return false;
      }
      inProgress = true;
      loginService.refreshLogin(function (value, httpHeaders) {
        $rootScope.accessToken = value.data.access_token;
        $rootScope.refreshToken = value.data.refresh_token;
        var actual_vence_date = new Date();
        actual_vence_date.setSeconds((parseInt(value.data.expires_in)));
        $rootScope.tokenExpires = actual_vence_date.toGMTString();
        document.cookie = "access_token=" + $rootScope.accessToken + "; expires=" + $rootScope.tokenExpires;
        document.cookie = "refresh_token=" + $rootScope.refreshToken + "; expires=" + $rootScope.tokenExpires;
        document.cookie = "token_expires=" + $rootScope.tokenExpires + "; expires=" + $rootScope.tokenExpires;

        //loginService.getUsersDetail(function(value_user, httpHeaders) {
        //	$rootScope.authenticatedUser = value_user.data;
        //	//message.show('success', "Sesión iniciada con éxito");
        //	//Falta echar proceso de cookies
        //	//$state.go('app.index.wellcome');
        //}, function(httpResponse) {
        //	console.log(httpResponse);
        //});

        inProgress = false;
      }, function (httpResponse) {
        inProgress = false;
        if (httpResponse.data.error_description == "Bad credentials") {

        } else {

        }
      });
    }
  }
]);

comerciantesApp.service('usersService', [
  '$q',
  '$resource',
  'APP_CONFIG',
  function ($q, $resource, config) {
    var paramDefault = {
      id: '@id'
    };
    var actions = {
      queryUsers: {
        method: 'GET',
        url: config().path.api.oauth2 + '/users',
        isArray: true
      },
      getUserDetails: {
        method: 'GET',
        url: config().path.api.oauth2 + '/userdetail',
        isArray: false
      },
      queryRoles: {
        method: 'GET',
        url: config().path.api.oauth2 + '/roles',
        isArray: true
      },
      update: {
        method: 'PUT',
        url: config().path.api.oauth2 + '/user'
      },
      changeUserPassword: {
        method: 'PUT',
        url: config().path.api.oauth2 + '/userPassword'
      },
      deleteNonStandard: {
        method: 'DELETE',
        url: config().path.api.oauth2 + '/user'
      }
    };
    return $resource(config().path.api.oauth2 + '/user/:id', paramDefault, actions);
  }
]);
