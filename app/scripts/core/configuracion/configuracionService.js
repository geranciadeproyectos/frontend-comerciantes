'use strict';
comerciantesApp = angular.module('bcscApp');

comerciantesApp.service("serviceConfiguracion",
  ["$http", '$q', 'APP_CONFIG',
    function ($http, $q, config) {
      this.getConfiguracion = function () {
        return $http({
          method: 'GET',
          url: config().path.api.configuracion.base
        });
      };
}]);
