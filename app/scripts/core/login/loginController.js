'use strict';
comerciantesApp.controller('LoginCtrl', [
  '$rootScope', '$scope', '$state', '$stateParams', 'loginService', 'serviceEmpresa', 'refrescarTokenUser', 'factoryData', 'mhFactoryLoader', "serviceManageError",
  function ($rootScope, $scope, $state, $stateParams, loginService, serviceEmpresa, refrescarTokenUser, $factoryData, $mhFactoryLoader, $serviceManageError) {

    $scope.model = {username: "sacastañedaa", password: "123456"};

    $scope.submitForm = function (form) {
      $factoryData.clearData("empresa");
      $factoryData.clearData("usuario");
      //console.log("Realizando autenticacion del usuario");
      loginService.login($scope.model, function (value, httpHeaders) {
        $rootScope.accessToken = value.data.access_token;
        $rootScope.refreshToken = value.data.refresh_token;
        var actual_vence_date = new Date();
        actual_vence_date.setSeconds((parseInt(value.data.expires_in)));
        $rootScope.tokenExpires = actual_vence_date.toGMTString();
        document.cookie = "access_token=" + $rootScope.accessToken + "; expires=" + $rootScope.tokenExpires;
        document.cookie = "refresh_token=" + $rootScope.refreshToken + "; expires=" + $rootScope.tokenExpires;
        document.cookie = "token_expires=" + $rootScope.tokenExpires + "; expires=" + $rootScope.tokenExpires;
        $state.go('app');
      }, function (httpResponse) {
        $mhFactoryLoader.hide();
        if ((httpResponse.data !== undefined || httpResponse.data !== null) && (httpResponse.data.error_description !== undefined || httpResponse.data.error_description !== null)) {
          if (httpResponse.data.error_description == "Bad credentials") {
            showAlertError("Usuario o contraseña incorrectos");
          }
        } else {
          showAlertError("Fallo en el inicio de Sesión");
        }
      });
    };

    $scope.alerts = [];
    function showAlertSucess(message) {
      $scope.alerts.push({
        type: 'success',
        msg: message
      });
    }

    function showAlertInfo(message) {
      $scope.alerts.push({
        type: 'info',
        msg: message
      });
    }

    function showAlertError(message) {
      $scope.alerts.push({
        type: 'danger',
        msg: message
      });
    }

    function showAlertWarning(message) {
      $scope.alerts.push({
        type: 'warning',
        msg: message
      });
    }

    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    angular.element(document).ready(function () {
      $mhFactoryLoader.hide();
    });
  }])
;
