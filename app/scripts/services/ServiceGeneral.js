'use strict';
comerciantesApp = angular.module('bcscApp');

comerciantesApp.directive("misterHouseHandsontable",
  ["$compile",
    function ($compile) {

      function link(scope, element, attrs) {
        var container = $(element);

        var data = scope.data;
        scope.settings.data = data;

        /**
         * Funcionalidad de la seleccion de varios elementos en la cabecera
         * de la tabla con el fin de agragar la funcionalidad de seleccionar
         * y deseleccionar todos los elementos de la columna
         */

        var isChecked = false;
        container.on("mousedown", "input.checker", function (event) {
          event.stopPropagation();
        });
        container.on("mouseup", "input.checker", function () {
          isChecked = !$(this).is(':checked');
          // console.log(isChecked);
          if (data) {
            angular.forEach(data, function (value) {
              value.active = isChecked;
            });
          }
          container.handsontable('render');
        });

        /**
         * Configuracion de las cabeceras de la tabla de handsontable
         * Se valida si el nombre de la cabecera es checkbox se convierte en
         * un elemento html input checkbox
         */

        var headers = scope.settings.colHeaders;
        scope.settings.colHeaders = function (col) {
          var value = headers[col];
          if (value === 'checkbox') {
            var txt = "<input type='checkbox' class='checker' ";
            txt += isChecked ? 'checked="checked"' : '';
            txt += ">";
            return txt;
          } else {
            return value;
          }
        };

        /**
         * Definicion de comportamiento de los datos despues del cambio de los
         * datos en las celdas de handsontable
         * @param change - string que especfica el tipo de cambio en los datos
         * @param source - objeto con las propiedades del tipo de cambio generado
         */

        scope.settings.afterChange = function (change, source) {
          // console.log("Source [" + source + "]");
          // console.log("Change ");
          // console.log(change);
          if (source && source !== 'loadData') {
            if (change) {
              angular.forEach(change, function (value) {
                var index = value[0];
                var attr = value[1];
                var oldValue = value[2];
                var newValue = value[3];
                if (oldValue !== newValue) {
                  if (attr !== 'active') {
                    if (scope.$parent.hasOwnProperty('update')) {
                      scope.$parent.update(index, attr, oldValue, newValue);
                    }
                  }
                  // else {
                  //   console.log("Emitiendo cambios");
                  //   scope.$emit('changedCheckedActiveHs', data[index]);
                  // }
                  scope.$apply();
                }
              });
            }
          }
        };

        scope.settings.columnSorting = true;
        scope.settings.sortIndicator = true;

        container.handsontable(scope.settings);
        if (!angular.isUndefined(attrs.scroll) && attrs.scroll === "true") {
          element.removeAttr('style');
          element.css(
            {
              display: 'block',
              overflow: 'hidden',
              width: '100%',
              height: 'auto'
            });
        } else {
          element.removeAttr('style');
          element.css(
            {
              display: 'block',
              width: '100%',
              height: 'auto'
            });
        }

        // console.log("Imprimiendo data para handosntable");
        // console.log(data);
        // console.log(scope.settings);
        if (!validate.isEmpty(data) && data.length > 0) {
          // console.log('el tamaño de la lista de los datos es mayor qque 0');
          container.handsontable('updateSettings', {data: scope.data});
          // console.log(scope.data, attrs);
          $compile(element.contents())(scope);
        }

        scope.$watchCollection('data', function (newCollection, oldCollection) {
          var mensaje = "";
          if (newCollection === oldCollection) {
            mensaje = "No hay actualizacion del modelo";
          } else if (oldCollection.length == 0 && newCollection) {
            // console.log(newCollection, attrs);
            data = newCollection;
            container.handsontable('updateSettings', {data: data});
            $compile(element.contents())(scope);
            mensaje = "Primer carga de informacion";
          } else if ((oldCollection && newCollection) && newCollection.length > oldCollection.length) {
            data = newCollection;
            container.handsontable('updateSettings', {data: data});
            $compile(element.contents())(scope);
            mensaje = "Se agrego un nuevo elemento al modelo";
          } else if ((oldCollection && newCollection) && newCollection.length < oldCollection.length) {
            data = newCollection;
            container.handsontable('updateSettings', {data: data});
            $compile(element.contents())(scope);
            mensaje = "Se elimino un elemento del modelo";
          } else if ((oldCollection && newCollection) && newCollection !== oldCollection) {
            data = newCollection;
            container.handsontable('updateSettings', {data: data});
            $compile(element.contents())(scope);
            mensaje = "Se actualizo el modelo de datos";
          }
          // console.log("====================Propiedades de las collecciones en el Watch ====================");
          // console.log("Collecion Anterior");
          // console.log(oldCollection);
          // console.log("Coleccion nueva");
          // console.log(newCollection);
          // console.log("Tipo de actualizacion");
          // console.log(mensaje);
          // console.log("====================================================================================");
        });

        scope.$watch('settings', function (newvalue){
          // console.log("Cambio de la configuracion", newvalue);
          container.handsontable('updateSettings',{ columns: newvalue.columns });
          container.handsontable('updateSettings',{ colHeaders: newvalue.colHeaders });
          container.handsontable('updateSettings',{ data: newvalue.data });
          $compile(element.contents())(scope);
        });
      }

      return {
        restrict: 'E',
        scope: {
          data: '=data',
          settings: '=settings'
        },
        link: link
      }
    }]);

comerciantesApp.directive('fileModel',
  ["$parse",
    function ($parse) {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var model = $parse(attrs['fileModel']);
          var modelSetter = model.assign;
          element.bind('change', function () {
            scope.$apply(function () {
              modelSetter(scope.$parent, element[0].files[0]);
              // console.log(scope);
            });
          });
        }
      };
    }]);

comerciantesApp.directive('selectpicker',
  ["$timeout",
    function ($timeout) {
      return {
        // replace:true,
        link: function (scope, element) {
          $timeout(function () {
            element.selectpicker();
          });
        }
      }
    }]);

comerciantesApp.factory("mhFactoryLoader",
  [
    function () {
      var $scope;
      var $element;

      var $loader = {};

      $loader.init = function (scope, element) {
        if (angular.isUndefined($scope)) {
          $scope = scope;
          $scope.text = 'Cargando Información. Espere un momento';
        }
        if (angular.isUndefined($element)) {
          $element = element;
          $($element).dimmer('hide');
        }
      };
      $loader.show = function (text) {
        if (!angular.isUndefined(text)) {
          $scope.text = text;
        }
        $($element).dimmer('show');
      };

      $loader.hide = function () {
        $scope.text = '';
        $($element).dimmer('hide');
      };
      return $loader;
    }]);

comerciantesApp.directive('mhLoader',
  ["$compile", "mhFactoryLoader",
    function ($compile, $factory) {
      return {
        restrict: 'E',
        templateUrl: 'views/template/loading.html',
        link: function (scope, element) {
          $factory.init(scope, element);
          //$compile(element.contents())(scope);
        }
      }
    }]);

comerciantesApp.service("serviceManageError",
  [ '$state',
    function ($state) {
      this.processError = function (response) {
        var error_message = '';
        if (response !== null) {
          if (response.error == null) {
            error_message = response.message;
          } else {
            error_message = response.message;
          }
        } else {
          error_message = 'No hay conexion con el servidor de la aplicación. contacte a soporte';

        }
        return error_message;
      }
    }
  ]);

comerciantesApp.directive('mhCalendar',
  ['$compile',
    function ($compile) {
      function link(scope, element) {
        $(element).html('<div id="body-horario"></div>');


        var elementCalendar = $("#body-horario");
        elementCalendar.fullCalendar({
          eventOverlap: false,
          slotEventOverlap: false,
          lang: 'es',
          header:false,
          hiddenDays: [0],
          height: 500,
          defaultView: 'agendaWeek',  //VISTA PRINCIPAL
          allDaySlot: false,  //desabilita la barra superior
          columnFormat: {week: 'dddd'}, // MOSTRAR SOLO LOS NOMBRE DE LAS COLUMNAS
          dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
          //==================================================================================================
          //======================================Config inicial==============================================
          eventStartEditable: true, //EVENTO DESPUES DE CREADO NO LO DEJA MOVER
          selectable: true, // Habilita la accion de seleccion de creacion de eventos
          selectHelper: true, // opcion para mostrar una guia en la accion de seleccion de creacion de evento
          selectOverlap: false, //bloquea el espacio de un evento apra que no sobreescriban sobre el evento existente
          //======================================insertar evento=============================================
          select: function (start, end) {
            elementCalendar.fullCalendar('removeEvents');
            if (start.format("dddd-DD-MM-YYYY") == end.format("dddd-DD-MM-YYYY")) {
              var event = {
                id: start.format() + "" + end.format(),
                title: "Abierto",
                start: start.format(),
                end: end.format()
              };
              scope.$apply(function () {
                scope.data.push(event);
              });
            }
          },
          eventRender: function (event, element1) {
            element1.prepend('<div><span class="fc-content" style="float: right;width: 10px;font-size: 15px;padding: 0px 5px;cursor: not-allowed;" >X</span></div>');
            element1.find(".fc-content").click(function () {
              // console.log("Tamaño del horario antes de eliminar el evento" + scope.data.length);
              elementCalendar.fullCalendar('removeEvents');
              angular.forEach(scope.data, function (value, key) {
                if (value.id === event.id
                // moment(value.start,"YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY-h:mm") === event.start.format("DD-MM-YYYY-h:mm") &&
                // moment(value.end,"YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY-h:mm") === event.end.format("DD-MM-YYYY-h:mm")
                ) {
                  scope.data.splice(key, 1);
                  scope.$apply();
                }
              });
              // console.log("Tamaño del horario despues de eliminar el evento" + scope.data.length);
            });

          },
          //====================================== Eventos de drag and drop =================================
          eventDrop: function (event, delta, revertFunc) {
            // console.log(event);
            if (event.start.format("dddd-DD-MM-YYYY") == event.end.format("dddd-DD-MM-YYYY")) {
              angular.forEach(scope.data, function (value, key) {
                if (value.id === event.id) {
                  var newEvent = {
                    id: event.start.format() + "" + event.end.format(),
                    title: "Abierto",
                    start: event.start.format(),
                    end: event.end.format()
                  };
                  scope.data.splice(key, 1);
                  scope.data.push(newEvent);
                  scope.$apply();
                }
              });
            } else {
              revertFunc();
            }
          }
        });
        if (!angular.isUndefined(scope.data) && scope.data.length > 0){
          elementCalendar.fullCalendar('removeEvents');
          elementCalendar.fullCalendar('addEventSource', scope.data);
        }
        scope.$watchCollection('data', function (newCollection, oldCollection) {
          // var mensaje = "";
          if (newCollection === oldCollection) {
            // mensaje = "No hay actualizacion del modelo";
            return;
          }
          if (!oldCollection && newCollection) {
            elementCalendar.fullCalendar('removeEvents');
            elementCalendar.fullCalendar('addEventSource', newCollection);
            // mensaje = "Primer carga de informacion";
          }
          if ((oldCollection && newCollection) && newCollection.length > oldCollection.length) {
            elementCalendar.fullCalendar('removeEvents');
            elementCalendar.fullCalendar('addEventSource', newCollection);
            // mensaje = "Se agrego un nuevo elemento al modelo";
          } else if ((oldCollection && newCollection) && newCollection.length < oldCollection.length) {
            elementCalendar.fullCalendar('removeEvents');
            elementCalendar.fullCalendar('addEventSource', newCollection);
            // mensaje = "Se elimino un elemento del modelo";
          } else if ((oldCollection && newCollection) && newCollection !== oldCollection) {
            elementCalendar.fullCalendar('removeEvents');
            elementCalendar.fullCalendar('addEventSource', newCollection);
            // mensaje = "Se actualizo el modelo de datos";
          }
          // console.log("====================Propiedades de las collecciones en el Watch ====================");
          // console.log("Collecion Anterior");
          // console.log(oldCollection);
          // console.log("Coleccion nueva");
          // console.log(newCollection);
          // console.log("Tipo de actualizacion");
          // console.log(mensaje);
          // console.log("====================================================================================");
        });
        $compile(element.contents())(scope);
      }

      return {
        restrict: 'E',
        scope: {
          data: '=data',
        },
        link: link
      }
    }]);
comerciantesApp.directive("misterHouseDateRangePicker",
  function ($compile) {
    function link(scope, element, attrs) {
      var container = $(element);
      container.html(
        '<div class="input-group">' +
        '<input type="text" name="daterange" class="form-control pull-right" value="" />' +
        '<div class="input-group-addon">' +
        '<i class="fa fa-calendar"></i>' +
        '</div>' +
        '</div>');
      var elementDatePicker = container.find('input');
      elementDatePicker.daterangepicker(
        {
          autoUpdateInput: false,
          locale: {
            format: 'YYYY-MM-DD',
            applyLabel: 'Aplicar',
            cancelLabel: 'Limpiar',
            daysOfWeek: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembere", "Octubre", "Noviembre", "Diciembre"],
            defaultDate: "",
            disabledDates: [],
          }
        },
        function (start, end, label) {
          scope[attrs.model][attrs.start] = start.format('YYYY-MM-DD');
          scope[attrs.model][attrs.end] = end.format('YYYY-MM-DD');
        });

      elementDatePicker.on('apply.daterangepicker', function (ev, picker) {
        elementDatePicker.val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
      });

      elementDatePicker.on('cancel.daterangepicker', function (ev, picker) {
        scope[attrs.model][attrs.start] = null;
        scope[attrs.model][attrs.end] = null;
        elementDatePicker.val('');
      });


      scope.$watch(attrs.model, function (newValue) {
        console.log("Cambio del valor de la fecha de inicio");
        console.log("New Value :" + newValue);
        var data = elementDatePicker.data('daterangepicker');
        //scope[attrs.model][attrs.start] || moment().format('YYYY-MM-DD'
        if (newValue[attrs.start] !== null && newValue[attrs.end] !== null) {
          data.setStartDate(newValue[attrs.start]);
          data.setEndDate(newValue[attrs.end]);
          elementDatePicker.val(newValue[attrs.start] + ' - ' + newValue[attrs.end]);
        } else {
          elementDatePicker.val('');
        }
      });
    };
    return {
      replace: true,
      restrict: 'E',
      link: link
    }
  });

comerciantesApp.directive("mhSemanticUiDropDown",
  function ($compile, $timeout) {
    function getRealScope(scope, attrs) {
      var final_instance = scope;
      var splAtrr = attrs.split('.');
      for (var i = 0; i < splAtrr.length; i++) {
        //splAtrr[i]
        if (final_instance.hasOwnProperty(splAtrr[i])) {
          final_instance = final_instance[splAtrr[i]];
        }
        else {
          final_instance = null;
          break;
        }
      }
      return final_instance;
    }

    function setRealScope(scope, attrs, newVal) {
      var final_instance = scope;
      eval('scope.' + attrs + ' = ' + JSON.stringify(newVal) + '');
    };

    function getOptCaption(arrAmps, objectOption) {
      var cap = '';
      for (var i = 0; i < arrAmps.length; i++) {
        if (cap !== '') {
          cap += ' - ';
        }
        cap += objectOption[arrAmps[i]];
      }
      return cap;
    };

    function link(scope, element, attrs) {
      var container = $(element);
      var data_options = getRealScope(scope, attrs.modelOptions);
      var data_options_val_map = attrs.valueMap.split(',');
      var data_group_by = null;
      var data_group_last_str = '';
      var data_group_object_acum = {};
      var data_group_array = [];
      var data_group_array_selected = [];
      var unique_fill = attrs.hasOwnProperty('uniqueVals') == true ? true : false;
      var unique_map_object = unique_fill == true ? attrs.uniqueVals : '';
      var raw_data = [];
      var model_choosed_change = false;
      var adding_or_removing = false;
      var adding = false;
      var removing = false;
      var addingObj = {};
      var removingObj = {};
      var watching_choosed = false;

      container.html(
        '<select class="ui fluid search dropdown" multiple="">' +
        '</select>'
      );

      var select_multiple = container.find('select');

      if (attrs.hasOwnProperty('groupByMap')) {
        data_group_by = attrs.groupByMap;

        data_options = data_options.sort(function (a, b) {
          if (a[data_group_by] < b[data_group_by]) return -1;
          if (a[data_group_by] > b[data_group_by]) return 1;
          return 0;
        });
      }

      function onlyUnique(value, index, self) {
        //var index_to_taked = chooseds_now.map(function(x) {return x._id; }).indexOf(data.room);
        //raw_data.map(function(x) {return x[unique_map_object]; }).indexOf(value[unique_map_object]);
        console.log(self.map(function (x) {
          return x[unique_map_object];
        }).indexOf(value[unique_map_object]));

        return self.map(function (x) {
            return x[unique_map_object];
          }).indexOf(value[unique_map_object]) === index;
      }

      function index_object_calc(arrObj, obj) {
        var index = -1;
        if (arrObj !== null) {
          for (var i = 0; i < arrObj.length; i++) {
            var equal = true;
            for (var j = 0; j < data_options_val_map.length; j++) {
              if (arrObj[i][data_options_val_map[j]] != obj[data_options_val_map[j]]) {
                equal = false;
              }
            }
            if (equal == true) {
              index = i;
              break;
            }

          }
        }
        return index;
      }

      function setArrOpts() {
        if (unique_fill == true) {
          raw_data = getRealScope(scope, attrs.modelOptions);
          data_options = raw_data.filter(onlyUnique);

        } else {
          data_options = getRealScope(scope, attrs.modelOptions);
        }
        var chooseds_now = getRealScope(scope, attrs.modelChoosed);

        var opts_string = '';
        select_multiple.empty();
        angular.forEach(data_options, function (value, key) {
          select_multiple.append($("<option></option>")
            .attr("value", key).text(getOptCaption(data_options_val_map, value)));

          var index_to_taked = index_object_calc(chooseds_now, value);
          if (index_to_taked > -1) {
            data_group_array_selected.push(key);
          }

        });

      }

      setArrOpts();

      select_multiple.dropdown(
        {
          placeholder : 'Escoge...',
          action : 'combo',
          message: {
            noResults: 'No se encontraron resultados.'
          },
          onChange: function(value, text, $selectedItem) {
            console.log(value);
            console.log('text: '+text);
            //console.log('search input: '+container.find('.search input').val());
            //console.log(select_multiple.dropdown('get text'));
            var chooseds_now = getRealScope(scope, attrs.modelChoosed) !== null ? getRealScope(scope, attrs.modelChoosed) : [];
            data_group_array_selected = [];
            angular.forEach(value, function (valuei, keyi) {
              var index = index_object_calc(chooseds_now, data_options[parseInt(valuei)]);
              if(index <= -1){
                var countUp = function() {
                  console.log("remove selected");
                  select_multiple.dropdown("remove selected", valuei);
                }

                $timeout(countUp, 10);
              }else{
                data_group_array_selected.push(valuei);
              }
            });

          },
          onAdd: function(addedValue, addedText, $addedChoice) {
            //adding = true;
            if(adding_or_removing == true){
              var chooseds_now = getRealScope(scope, attrs.modelChoosed) !== null ? getRealScope(scope, attrs.modelChoosed) : [];
              var index = index_object_calc(chooseds_now, data_options[parseInt(addedValue)]);
              if(index <= -1){
                chooseds_now.push(data_options[parseInt(addedValue)]);
                setRealScope(scope, attrs.modelChoosed, chooseds_now);
              }
              //adding = false;
              seteSelecteds();
              adding_or_removing = false;
            }

          },
          onRemove: function(removedValue, removedText, $removedChoice){
            var chooseds_now = getRealScope(scope, attrs.modelChoosed);
            var index = index_object_calc(chooseds_now, data_options[parseInt(removedValue)]);
            if(index > -1){
              chooseds_now.splice(index, 1);
              setRealScope(scope, attrs.modelChoosed, chooseds_now);
            }
            removing = false;
            seteSelecteds();

          }
        }
      );

      var menu_drop = container.find('.dropdown');
      var search_drop = container.find('.dropdown .search');

      menu_drop.mouseup(function() {
        adding_or_removing = true;
        console.log( "Handler for .mouseup() called." );

      });

      menu_drop.mouseout(function() {
        adding_or_removing = false;
        console.log( "Handler for .mouseout() called." );

      });

      function seteSelecteds(){
        var chooseds_now = getRealScope(scope, attrs.modelChoosed);
        data_group_array_selected = [];
        angular.forEach(data_options, function (value, key) {
          var index_to_taked = index_object_calc(chooseds_now, value);
          if (index_to_taked > -1) {
            if(data_group_array_selected.indexOf(key) <= -1){
              data_group_array_selected.push(key.toString());
            }
          }
        });
        //select_multiple.dropdown("clear");
        select_multiple.dropdown("set selected", data_group_array_selected);
      }

      //menu_drop.mouseout(function() {
      //  console.log( "Handler for .mouseout() called." );
      //});

      scope.$watchCollection(attrs.modelOptions, function (newCollection, oldCollection) {
        if (newCollection !== oldCollection) {
          setArrOpts();
          select_multiple.dropdown("refresh");
          select_multiple.dropdown("set selected", data_group_array_selected);
        }
      });

      scope.$watchCollection(attrs.modelChoosed, function (newCollection, oldCollection) {
        console.log('changed choosed');
        if (newCollection !== oldCollection) {

          var chooseds_now = getRealScope(scope, attrs.modelChoosed);
          model_choosed_change = true;
          data_group_array_selected = [];
          angular.forEach(data_options, function (value, key) {
            var index_to_taked = index_object_calc(chooseds_now, value);
            if (index_to_taked > -1) {
              if (data_group_array_selected.indexOf(key) <= -1) {
                data_group_array_selected.push(key.toString());
              }
            }
          });
          watching_choosed = true;
          select_multiple.dropdown("clear");
          select_multiple.dropdown("set selected", data_group_array_selected);

        }
      });

    }

    return {
      restrict: 'EA',
      link: link
    }
  });

comerciantesApp.directive("mhAlselect2",
  function ($compile) {
    function getRealScope(scope, attrs) {
      var final_instance = scope;
      var splAtrr = attrs.split('.');
      for (var i = 0; i < splAtrr.length; i++) {
        //splAtrr[i]
        if (final_instance.hasOwnProperty(splAtrr[i])) {
          final_instance = final_instance[splAtrr[i]];
        }
        else {
          final_instance = null;
          break;
        }
      }
      return final_instance;
    }

    function setRealScope(scope, attrs, newVal) {
      var final_instance = scope;
      //var splAtrr = attrs.split('.');
      //eval('scope.'+attrs+' = new Date('+newVal+')');
      eval('scope.' + attrs + ' = ' + JSON.stringify(newVal) + '');
    };

    function getOptCaption(arrAmps, objectOption) {
      var cap = '';
      for (var i = 0; i < arrAmps.length; i++) {
        if (cap !== '') {
          cap += ' - ';
        }
        cap += objectOption[arrAmps[i]];
      }
      return cap;
    }

    function link(scope, element, attrs) {
      var container = $(element);
      var data_options = getRealScope(scope, attrs.modelOptions);
      var data_options_val_map = attrs.valueMap.split(',');
      var data_group_by = null;
      var data_group_last_str = '';
      var data_group_object_acum = {};
      var data_group_array = [];
      var data_group_array_selected = [];
      var unique_fill = attrs.hasOwnProperty('uniqueVals') == true ? true : false;
      var unique_map_object = unique_fill == true ? attrs.uniqueVals : '';
      var raw_data = [];
      var model_choosed_change = false;

      if (attrs.hasOwnProperty('groupByMap')) {
        data_group_by = attrs.groupByMap;

        data_options = data_options.sort(function (a, b) {
          if (a[data_group_by] < b[data_group_by]) return -1;
          if (a[data_group_by] > b[data_group_by]) return 1;
          return 0;
        });
      }

      var opt_str_html = '';

      function onlyUnique(value, index, self) {
        //var index_to_taked = chooseds_now.map(function(x) {return x._id; }).indexOf(data.room);
        //raw_data.map(function(x) {return x[unique_map_object]; }).indexOf(value[unique_map_object]);
        console.log(self.map(function (x) {
          return x[unique_map_object];
        }).indexOf(value[unique_map_object]));

        return self.map(function (x) {
            return x[unique_map_object];
          }).indexOf(value[unique_map_object]) === index;
      }

      function index_object_calc(arrObj, obj) {
        var index = -1;
        if (arrObj !== null) {
          for (var i = 0; i < arrObj.length; i++) {
            var equal = true;
            for (var j = 0; j < data_options_val_map.length; j++) {
              if (arrObj[i][data_options_val_map[j]] != obj[data_options_val_map[j]]) {
                equal = false;
              }
            }
            if (equal == true) {
              index = i;
              break;
            }

          }
        }

        return index;
      }

      function setArrOpts() {
        if (unique_fill == true) {
          raw_data = getRealScope(scope, attrs.modelOptions);
          data_options = raw_data.filter(onlyUnique);

        } else {
          data_options = getRealScope(scope, attrs.modelOptions);
        }
        var chooseds_now = getRealScope(scope, attrs.modelChoosed);
        //acomodar opciones
        for (var i = 0; i < data_options.length; i++) {
          if (data_group_by !== null) {

            if (!data_group_object_acum.hasOwnProperty(data_options[i][data_group_by])) {
              data_group_array.push(
                {
                  text: data_options[i][data_group_by],
                  children: []
                }
              );

              data_group_object_acum[data_options[i][data_group_by]] = 'taked';
            }
            data_group_array[data_group_array.length - 1].children.push({
              id: i,
              text: getOptCaption(data_options_val_map, data_options[i])
            });
          } else {
            data_group_array.push(
              {
                id: i,
                text: getOptCaption(data_options_val_map, data_options[i])
              }
            );

          }
          //var index_to_taked = chooseds_now.indexOf(data_options[i]);
          var index_to_taked = index_object_calc(chooseds_now, data_options[i]);
          if (index_to_taked > -1) {
            data_group_array_selected.push(i);
          }
        }

      }

      setArrOpts();

      container.html(
        '<div class="form-group">' +
        '<select class="form-control select2" multiple="multiple">' +
        '</select>' +
        '</div>'
      );

      var elementSelect2 = container.find('select');

      elementSelect2.select2(
        {
          placeholder: "Escoge ...",
          allowClear: false,
          minimumInputLength: 0,
          multiple: true,
          data: data_group_array
        }
      );

      elementSelect2.val(data_group_array_selected).trigger("change");

      elementSelect2.on("change", function (e) {
        //console.log('change: ');
        console.log(e.target.selectedOptions);
        //setRealScope(scope, attrs.modelChoosed, []);
        //console.log(elementSelect2.select2('val'));
        //for (var i = 0; i < e.target.selectedOptions.length; i++) {
        //  var sel_index = parseInt(e.target.selectedOptions[i].index);
        //  data_choosed.push(data_options[sel_index]);
        //}
        if (model_choosed_change == false) {
          var data_choosed = [];
          angular.forEach(e.target.selectedOptions, function (value, key) {
            var sel_index = parseInt(e.target.selectedOptions[key].index);
            data_choosed.push(data_options[sel_index]);
          });
          setRealScope(scope, attrs.modelChoosed, data_choosed);
        } else {
          model_choosed_change = false;
        }
        if (attrs.hasOwnProperty('changeVals')) {
          if (scope.hasOwnProperty(attrs.changeVals)) {
            eval('scope.' + attrs.changeVals + '()');
          }
        }
      });

      scope.$watchCollection(attrs.modelOptions, function (newCollection, oldCollection) {
        if (newCollection !== oldCollection) {
          setArrOpts();
          elementSelect2.select2('destroy');
          elementSelect2.select2(
            {
              placeholder: "Escoge ...",
              allowClear: false,
              minimumInputLength: 0,
              multiple: true,
              data: data_group_array
            }
          );

          elementSelect2.val(data_group_array_selected).trigger("change");

        }
      });

      scope.$watchCollection(attrs.modelChoosed, function (newCollection, oldCollection) {
        if (newCollection !== oldCollection) {
          //console.log('newCollection: ');
          //console.log(newCollection);
          //console.log('oldCollection: ');
          //console.log(oldCollection);
          var chooseds_now = getRealScope(scope, attrs.modelChoosed);
          model_choosed_change = true;
          angular.forEach(data_options, function (value, key) {
            var index_to_taked = index_object_calc(chooseds_now, value);
            if (index_to_taked > -1) {
              if (data_group_array_selected.indexOf(key) <= -1) {
                data_group_array_selected.push(key);
              }
            }
          });
          elementSelect2.val(data_group_array_selected).trigger("change");
          //elementSelect2.select2('val', data_group_array_selected);
        }
      });

    }

    return {
      restrict: 'EA',
      link: link
    }
  });

comerciantesApp.directive("ngFileSelect",function(){
  return {
    link: function($scope,el){
      el.bind("change", function(e){

        $scope.file = (e.srcElement || e.target).files[0];
        $scope.getFile($scope.file);
      })
    }

  }
});
