'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.factory("factoryData",
  [
    function () {

      this.data = {};
      this.defaultData = {};
      this.removeDataIndex = function (nameData, index) {
        var dataTmp = this.data[nameData][index];
        if (!validate.isEmpty(dataTmp._status) && dataTmp._status ==="create") {
          this.data[nameData].splice(index, 1);
        } else {
          dataTmp._status = "delete";
        }
      };

      this.removeData = function (nameData, nameFieldId, data, index) {
        // console.log("removiendo el dato", data);
        var indexTmp = undefined;
        if (data[nameFieldId] !== null) {
          // console.log("El codigo no es nulo : ", data[nameFieldId]);
          angular.forEach(this.data[nameData], function (value, key) {
            // console.log("validación de codigos :", value.codigo, data.codigo);
            if (value[nameFieldId] === data[nameFieldId]) {
              if (!angular.isUndefined(value._status)) {
                // console.log("se elimianra un objeto que contenia un status");
                if (value._status === "create") {
                  indexTmp = key;
                } else {
                  value._status = "delete";
                  if (angular.isUndefined(value._id)) {
                    value._id = data.codigo;
                  }
                }
              } else {
                // console.log("Se eliminara un elemento que tiene codigo y no tenia status");
                value._status = "delete";
                if (angular.isUndefined(value._id)) {
                  value._id = data[nameFieldId];
                }
              }
              // console.log("objeto encontrado : ", value);
            }
          });
        }
        if (!angular.isUndefined(indexTmp)) {
          this.data[nameData].splice(indexTmp, 1);
        } else if (this.data[nameData][index]._status ==="create") {
          this.data[nameData].splice(index, 1);
        }
        // console.log(this.data[nameData].length, this.data[nameData]);
      };

      this.updateData = function (nameData, nameFieldId, valueFieldId, attr, oldValue, newValue, index) {
        console.log("Actualización desde la factoria");
        // console.log(this.data[nameData]);
        console.log(nameFieldId, valueFieldId, attr, oldValue, newValue);
        var match = false;
        if(validate.isEmpty(valueFieldId) && validate.isEmpty(oldValue) && !validate.isEmpty(newValue)){
          this.data[nameData][index][attr] = newValue;
        }else {
          angular.forEach(this.data[nameData], function (value, key) {
            if (value[nameFieldId] === valueFieldId) {
              console.log(key, value);
              match = true;
              if (attr !== nameFieldId) {
                console.log("Actualizando el atributo de un registro", nameFieldId, valueFieldId, attr, oldValue, newValue);
                if (angular.isUndefined(value._status)) {
                  value._status = "update";
                  value[attr] = newValue;
                  if (angular.isUndefined(value._id)) {
                    value._id = valueFieldId;
                  }
                } else if (value._status === "create") {
                  // console.log("Actualizando registro que fue previamente actualizado");
                  value[attr] = newValue;

                } else {
                  value[attr] = newValue;
                  value._status = "update";
                  if (angular.isUndefined(value._id)) {
                    value._id = valueFieldId;
                  }
                }
              } else {
                console.log("Se realiza un cambio sobre el id :", nameFieldId, valueFieldId, attr, oldValue, newValue);
                if (!angular.isUndefined(value._status) && value._status === "create") {
                  console.log("Actualizando el atributo del codigo : ", newValue);
                  value[attr] = newValue;
                  value._id = newValue;
                } else if (!angular.isUndefined(value._status) && value._status === "update") {
                  // console.log("Actualizando el atributo del codigo y ya fue actualizado previamente : ", newValue);
                  value[attr] = newValue;
                  if (angular.isUndefined(value._id)) {
                    value._id = newValue;
                  }
                } else if (angular.isUndefined(value._status)) {
                  // console.log("Actualizando el atributo del codigo y no fue actualizado previamente  : ", newValue);
                  value._id = oldValue;
                  value._status = "update";
                  value[attr] = newValue;
                }
              }
              // console.log(value);
            }
          });
          if (!match) {
            console.log("No encontro el registro en la fabrica", this.data[nameData][index]);
            this.data[nameData][index][attr] = newValue;
            if (validate.isEmpty(this.data[nameData][index]._status)) {
              this.data[nameData][index]._status = "update";
            }
          }
        }
      };

      this.addData = function (nameData, data) {
        var dataTmp = JSON.parse(JSON.stringify(data));
        dataTmp._status = "create";
        this.data[nameData].unshift(dataTmp);
      };

      this.getDataWithoutRemoved = function (nameData) {
        var dataTmp = [];
        angular.forEach(this.data[nameData], function (value) {
          if (angular.isUndefined(value._status) || value._status !== "delete") {
            dataTmp.push(value);
          }
        });
        return JSON.parse(JSON.stringify(dataTmp));
      };

      this.getDataByNameAttribute = function (nameData, nameFieldId, valueFielId, nameAttribute) {
        // console.log("Obtenientdo información del objeto:", nameFieldId, valueFielId, nameAttribute, this.data[nameData]);
        var index = undefined;
        angular.forEach(this.getDataWithoutRemoved(nameData), function (value, key) {
          if (value[nameFieldId] === valueFielId) {
            index = key;
          }
        });
        if (!angular.isUndefined(index)) {
          return this.data[nameData][index][nameAttribute];
        } else {
          return undefined;
        }
      };

      this.getDataFilterContainsByAttribute = function (nameData, nameFieldFilter, valueFieldFilter) {
        var dataTemp = [];
        angular.forEach(this.getDataWithoutRemoved(nameData), function (value) {
          // console.log(value[nameFieldFilter]+'',valueFieldFilter,value[nameFieldFilter].toString().contains(valueFieldFilter),value[nameFieldFilter].toString().indexOf(valueFieldFilter));
          if (!validate.isEmpty(value[nameFieldFilter]) && value[nameFieldFilter].toString().contains(valueFieldFilter)) {
            dataTemp.push(value);
          }
        });
        return JSON.parse(JSON.stringify(dataTemp));
      };

      this.existData = function (nameData, nameFieldFilter, valueFieldFilter) {
        var match = false;
        angular.forEach(this.getDataWithoutRemoved(nameData), function (value) {
          if (value[nameFieldFilter] === valueFieldFilter) {
            match = true;
          }
        });
        return match;
      };

      this.setDataByNameAttribute = function (nameData, nameFieldId, valueFielId, nameAttribute, data) {
        // console.log("Cambiando información del objeto : ", nameFieldId, valueFielId, nameAttribute, data, this.data[nameData]);
        if (!angular.isUndefined(data) && data !== null) {
          angular.forEach(this.data[nameData], function (value) {
            // var index;
            if (value[nameFieldId] === valueFielId) {
              // index = key;
              value[nameAttribute] = data;
            }
          });
          // console.log(this.data[nameData]);
        }
      };

      this.clearData = function (nameData) {
        this.data[nameData] = [];
      };

      this.clearDefaultData = function (nameData) {
        this.defaultData[nameData] = {};
      };

      this.getData = function (nameData) {
        return JSON.parse(JSON.stringify(this.data[nameData]));
      };

      this.setData = function (nameData, data) {
        this.data[nameData] = JSON.parse(JSON.stringify(data));
      };

      this.getDataDefault = function (nameData) {
        return JSON.parse(JSON.stringify(this.defaultData[nameData]));
      };

      this.setDataDefault = function (nameData, dataDefault) {
        this.defaultData[nameData] = JSON.parse(JSON.stringify(dataDefault));
      };

      this.getStateOperationData = function (nameData) {
        var stateOperationData = {
          delete: [],
          create: [],
          update: {
            ids: [],
            data: []
          }
        };

        angular.forEach(this.data[nameData], function (value) {
          // console.log(key);
          if (!angular.isUndefined(value._status)) {
            var valueTmp;
            if (value._status === "delete") {
              // console.log('value objectsDelete', value);
              stateOperationData.delete.push(value._id);
            }
            if (value._status === "update") {
              // console.log('value objectsUpdate', value);
              valueTmp = JSON.parse(JSON.stringify(value));
              delete valueTmp._id;
              delete valueTmp.active;
              delete valueTmp._status;
              delete valueTmp.horario;
              delete valueTmp.turnos;
              stateOperationData.update.ids.push(value._id);
              stateOperationData.update.data.push(valueTmp);
            }
            if (value._status === "create") {
              // console.log('value objectsCreate', value);
              valueTmp = JSON.parse(JSON.stringify(value));
              delete valueTmp._id;
              delete valueTmp.active;
              delete valueTmp._status;
              delete valueTmp.horario;
              delete valueTmp.turnos;
              stateOperationData.create.push(value);
            }
          }
        });
        console.log("Estado de cambios en los datos de" + nameData + " : ", stateOperationData);
        return stateOperationData;
      };
      return this;
    }]);
