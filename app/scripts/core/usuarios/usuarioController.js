'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.controller('usuarioController',
  ["$scope", "$rootScope", "$compile", "$uibModal", "serviceUsuario", "factoryData", "$state", 'mhFactoryLoader', "serviceManageError", 'fileReader',
    function ($scope, $rootScope, $compile, $uibModal, $serviceUsuario, $factoryData, $state, $mhFactoryLoader, $serviceManageError, fileReader) {
      var nameData = "usuario";
      $scope.attrId = "username";
      var attrFilter = "nombre";
      $scope.currentUsuario = $rootScope.currentUsuario;
      var constraints = {};


      $factoryData.setDataDefault(nameData, {
        uuid: "",
        username: "",
        password: "",
        usuario: {
          uuid: "",
          nombre: "",
          cedula: "",
          rol: {
            uuid: "",
            nombre: ""
          }
        }
      });
      $scope.page = {
        title: "Información de Usuarios",
        buttons: [
          {
            show: 'noExisteFilterValue',
            title: 'Refrescar Usuarios',
            action: 'refresh',
            icon: 'fa-refresh'
          },
          {
            show: 'noExisteFilterValue',
            title: 'Adicionar nuevo Usuario',
            action: 'openCrearUsuario',
            icon: 'fa-plus-circle'
          },
        ],
        modal: {
          create: {
            title: "Crear Usuario",
          },
          edit: {
            title: "Editar Usuario",
          }
        }
      };

      $scope.data = $factoryData.getDataWithoutRemoved(nameData);
      $scope.dataUsuarios = [];
      $scope.statusSave = 0;
      $scope.statusLoad = 0;
      $scope.filterValue = "";
      $scope.newUser = {};
      $scope.editData = {};

      $scope.callFunction = function (name, params) {
        if (angular.isFunction($scope[name])) {
          if (validate.isEmpty(params)) {
            return $scope[name]();
          } else {
            return $scope[name](params);
          }
        } else {
          return name;
        }
      };

      $scope.showOperation = function (cuenta){
        $scope.currentUsuario = $rootScope.currentUsuario;

        if (validate.isEmpty($scope.currentUsuario)){
          return false;
        }
        if(cuenta.usuario.rol.nombre==='ADMINISTRADOR PRINCIPAL'){
          return false;
        }
        if ($scope.currentUsuario.usuario.rol.nombre==='ADMINISTRADOR' && cuenta.usuario.rol.nombre==='ADMINISTRADOR'){
          return false;
        }
          return true;
      };
      /**
       * Operacion que recarga la informacion de los Distribuidors
       */
      $scope.refresh = function () {
        $mhFactoryLoader.show("Refrescando la información de los Distribuidors registrados");
        $scope.getAllData(true);
      };

      /**
       * Metodo que retorna los Distribuidors registrados en el api
       */
      $scope.getAllData = function (showMessage) {
        $factoryData.clearData(nameData);
        $serviceUsuario.getAll()
          .success(function (response) {
            if (!validate.isEmpty(showMessage) && showMessage) {
              showAlertSucess('Se cargaron ' + response.length + ' usuarios correctamente');
            }
            $factoryData.setData(nameData, response);
            if (!$scope.noExisteFilterValue()) {
              $scope.data = $factoryData.getDataFilterContainsByAttribute(nameData, attrFilter, $scope.filterValue);
            } else {
              $scope.data = $factoryData.getDataWithoutRemoved(nameData);
            }
            $scope.statusLoad++;
          })
          .error(function (error) {
            showAlertError($serviceManageError.processError(error));
            $scope.statusLoad++;
          });
      };

      $scope.save = function () {
        console.log($scope.newUser);

        var constraints = {
          "usuario.nombre": {

            presence: {message: "^El nombre de usuario es un valor obligatorio y no puede estra vacio"}
          },
          "usuario.cedula": {
            presence: {message: "^El número de cédula es un valor obligatorio y no puede estra vacio"},
            numericality: {message: "^El número de cédula debe ser un valor númerico no negativo", greaterThan: 0}
          },
          username: {
            presence: {message: "^la cuenta de usuario es un valor obligatorio y no puede estra vacio"}
          },
          password: {
            presence: {message: "^La contraseña es un valor obligatorio y no puede estra vacio"},
            equality: {
              attribute: "passwordValidate",
              message: "^Las contraseñas no son iguales"
            }
          },
          passwordValidate: {
            presence: {message: "^La confirmación de contraseña es un valor obligatorio y no puede estra vacio"},
          },
          "usuario.rol.nombre": {
            presence: {message: "^El rol del usuario es un valor obligatorio"},
            equality: {
              attribute: "usuario.rol.nombreOtro",
              message: "^Debe escribir un valor para el rol que desea adicionar al usuario",
              comparator: function (v1, v2) {
                console.log(v1, v2);
                if (v1 == "Otro" && validate.isEmpty(v2)) {
                  return false;
                } else {
                  return true;
                }
              }
            }
          }
        };
        $scope.errorValidation = $scope.validateData($scope.newUser, constraints);
        if ($scope.errorValidation.length === 0) {
          if ($scope.newUser.usuario.rol.nombre==='Otro'){
            $scope.newUser.usuario.rol.nombre = $scope.newUser.usuario.rol.nombreOtro;
            delete $scope.newUser.usuario.rol.nombreOtro;
          }
          delete $scope.newUser.passwordValidate;
          $mhFactoryLoader.show('Realizando la creación de usuario');
          $serviceUsuario.create($scope.newUser)
            .success(function () {
              $scope.cancelModal();
              $scope.getAllData();
            })
            .error(function (error) {
              $mhFactoryLoader.hide();
              showAlertError($serviceManageError.processError(error));
            });

        } else {
          $scope.openModalValidateError();
        }
      };

      $scope.update = function () {
        console.log($scope.editUser);
        var constraints = {
          "usuario.nombre": {

            presence: {message: "^El nombre de usuario es un valor obligatorio y no puede estra vacio"}
          },
          "usuario.cedula": {
            presence: {message: "^El número de cédula es un valor obligatorio y no puede estra vacio"},
            numericality: {message: "^El número de cédula debe ser un valor númerico no negativo", greaterThan: 0}
          },
          username: {
            presence: {message: "^la cuenta de usuario es un valor obligatorio y no puede estra vacio"}
          },
          password: {
            presence: {message: "^La contraseña es un valor obligatorio y no puede estra vacio"},
            equality: {
              attribute: "passwordValidate",
              message: "^Las contraseñas no son iguales"
            }
          },
          passwordValidate: {
            presence: {message: "^La confirmación de contraseña es un valor obligatorio y no puede estra vacio"},
          },
          "usuario.rol.nombre": {
            presence: {message: "^El rol del usuario es un valor obligatorio"},
            equality: {
              attribute: "usuario.rol.nombreOtro",
              message: "^Debe escribir un valor para el rol que desea adicionar al usuario",
              comparator: function (v1, v2) {
                console.log(v1, v2);
                if (v1 == "Otro" && validate.isEmpty(v2)) {
                  return false;
                } else {
                  return true;
                }
              }
            }
          }
        };
        $scope.errorValidation = $scope.validateData($scope.editUser, constraints);
        if ($scope.errorValidation.length === 0) {
          if ($scope.editUser.usuario.rol.nombre==='Otro'){
            $scope.editUser.usuario.rol.nombre = $scope.editUser.usuario.rol.nombreOtro;
            delete $scope.editUser.usuario.rol.nombreOtro;
          }
          delete $scope.editUser.passwordValidate;
          $mhFactoryLoader.show('Realizando la actualización del usuario');
          $serviceUsuario.update($scope.editUser)
            .success(function () {
              $scope.cancelModal();
              $scope.getAllData();
            })
            .error(function (error) {
              $mhFactoryLoader.hide();
              showAlertError($serviceManageError.processError(error));
            });
        } else {
          $scope.openModalValidateError();
        }
      };

      $scope.delete = function (id) {
        $mhFactoryLoader.show('Realizando eliminación del usuario');
        $serviceUsuario.delete(id)
          .success(function () {
            $scope.getAllData();
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });
      };

      $scope.openModalValidateError = function () {
        $scope.modalError = $uibModal.open({
          animation: true,
          backdrop: false,
          keyboard: false,
          scope: $scope,
          templateUrl: 'validacionDatos.html'
        });
      };

      $scope.cancelModalError = function () {
        $scope.modalError.close();
      };

      $scope.openCrearUsuario = function () {
        $serviceUsuario.getAllRoles()
          .success(function (data) {
            $scope.roles = data;
            $scope.roles.push({nombre: "Otro"});
            $scope.modal = $uibModal.open({
              animation: true,
              backdrop: false,
              keyboard: false,
              scope: $scope,
              templateUrl: 'crearUsuario.html'
            });
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });

      };

      $scope.openEditarUsuario = function (index) {
        $scope.editUser = JSON.parse(JSON.stringify($scope.data[index]));
        $scope.editUser.passwordValidate = $scope.editUser.password;
        $serviceUsuario.getAllRoles()
          .success(function (data) {
            $scope.roles = data;
            $scope.roles.push({nombre: "Otro"});
            $scope.modal = $uibModal.open({
              animation: true,
              backdrop: false,
              keyboard: false,
              scope: $scope,
              templateUrl: 'editarUsuario.html'
            });
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });
      };

      $scope.cancelModal = function () {
        $scope.uuidEmpresa = null;
        $scope.newUser = {};
        $scope.editUser = {};
        $scope.editData = {};
        $scope.newUser = {};
        $scope.file = null;
        $scope.imageSrc = null;
        $scope.modal.close();
      };

      $scope.noExisteCambios = function () {
        var existeCambios = false;
        angular.forEach($factoryData.getData(nameData), function (value) {
          if (!validate.isEmpty(value._status)) {
            existeCambios = true;
          }
        });
        return existeCambios;
      };

//================================operaciones desde memoria======================
      $scope.noExisteFilterValue = function () {
        return (validate.isEmpty($scope.filterValue));
      };

      $scope.filter = function () {
        if (!$scope.noExisteFilterValue()) {
          $scope.data = $factoryData.getDataFilterContainsByAttribute(nameData, attrFilter, $scope.filterValue);
        } else {
          $scope.data = $factoryData.getDataWithoutRemoved(nameData);
        }
      };

      $scope.changeFilter = function () {
        $scope.filter();
      };

      $scope.validateData = function (data, constraints) {

        console.log("validando los datos");
        var errorsValidation = [];
        var error = validate(data, constraints);
        console.log("error : ", error);
        if (!angular.isUndefined(error)) {
          angular.forEach(error, function (value, attr) {
            errorsValidation.push({attr: attr, error: value.join()})
          });
        }
        // console.log("Errores de validacion de datos :", errorsValidation );
        return errorsValidation;
      };


      $scope.alerts = [];
      function showAlertSucess(message) {
        $scope.alerts.push({
          type: 'success',
          msg: message
        });
      }

      function showAlertInfo(message) {
        $scope.alerts.push({
          type: 'info',
          msg: message
        });
      }

      function showAlertError(message) {
        $scope.alerts.push({
          type: 'danger',
          msg: message
        });
      }

      function showAlertWarning(message) {
        $scope.alerts.push({
          type: 'warning',
          msg: message
        });
      }

      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      //====================================== FIN ==============================================//

      $scope.$watch("statusSave", function () {
        // console.log("StatusSave ",$scope.statusSave);
        if ($scope.statusSave >= 3) {
          $mhFactoryLoader.hide();
          $scope.statusSave = 0;
          $scope.filterValue = "";
          showAlertSucess("Se guardaron los cambios correctamente");
          $scope.getAllData();
        }
      });

      $scope.$watch("statusLoad", function () {
        // console.log("Valor del estado de carga de datos", $scope.statusLoad);
        if ($scope.statusLoad >= 1) {
          $mhFactoryLoader.hide();
          $scope.statusLoad = 0;
        }
      });

      angular.element(document).ready(function () {
        if ($factoryData.getDataWithoutRemoved(nameData).length == 0) {
          $mhFactoryLoader.show("Obteniendo información de los usuarios registrados");
          $scope.getAllData(true);
        }
      });
    }]);
