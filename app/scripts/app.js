'use strict';

/**
 * @ngdoc overview
 * @name comerciantesApp
 * @description
 * # balanceoDeCajaApp
 *
 * Main module of the application.
 */
/*
 var comerciantesApp =  angular.module('bcscApp', [
 'ui.router',
 'ngAnimate',
 'angular.filter',
 'ui.bootstrap',
 'ngMessages',
 'masterGeocoding',
 'bootstrap-tagsinput',
 'ngResource',
 'misterhouse'
 ]);
 */
var comerciantesApp = angular.module('bcscApp', [
  'ui.router',
  'ngAnimate',
  'angular.filter',
  'ui.bootstrap',
  'ngMessages',
  'bootstrap-tagsinput',
  'ngResource',
  'misterhouseAngular'
]);

comerciantesApp.config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');
    $stateProvider
      .state('app', {
        url: '/app',
        templateUrl: 'views/template/app.html'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'views/login/login-user.html',
        controller: 'LoginCtrl'
      })
      .state('app.usuarios', {
        url: '/usuarios',
        templateUrl: 'views/informacion/admin-usuario.html',
        controller: 'usuarioController',
        controllerAs: 'usuarioController'
      })

      .state('app.empresa', {
        url: '/empresas',
        templateUrl: 'views/informacion/admin-empresa.html',
        controller: 'empresaController',
        controllerAs: 'empresaController'
      })

      .state('app.administracion', {
        url: '/gestion-de-datos',
        templateUrl: 'views/informacion/administracion.html',
        controller: 'administracionController',
        controllerAs: 'administracionController'
      })

      .state('app.crearReportes', {
        url: '/crear-reportes',
        templateUrl: 'views/informacion/crear-reporte.html',
        controller: 'reporteController',
        controllerAs: 'reporteController'
      })

      .state('app.reportes', {
        url: '/reportes',
        templateUrl: 'views/informacion/reportes.html',
        controller: 'reporteController',
        controllerAs: 'reporteController'
      })


  }]);
// comerciantesApp.config(['APP_REQUIRES', function ( APP_REQUIRES) {
//   'use strict';
//     modules: APP_REQUIRES.modules;
//
// }]);

comerciantesApp.config(['$httpProvider', function ($httpProvider) {
  $httpProvider.interceptors.push('authRequestInterceptor');
  $httpProvider.interceptors.push('authResponseInterceptor');
}]);

comerciantesApp.run(['$rootScope', '$state', '$templateCache', '$http', 'loginService', 'refrescarTokenUser', function ($rootScope, $state, $templateCache, $http, loginService, refrescarTokenUser) {

  $rootScope.$state = $state;
  //$rootScope.setting = setting;
  //Este manejador de evento se coloco para eliminar todos los tooltips que
  //quedaron activos al validar un formulario debido a que la plantilla no
  //tiene este comportamiento por defecto.
  $rootScope.$on('$stateChangeStart',
    function (event, toState, toParams, fromState, fromParams, options) {
      console.log("Detectado cambio de estado de la navegacion");
      $('form *').tooltip('destroy');
      //cookies se salta el login por que esta ya esta session corriendo
      if (getCookie("access_token") != '') {
        $rootScope.accessToken = getCookie("access_token");
      }
      if (getCookie("refresh_token") != '') {
        $rootScope.refreshToken = getCookie("refresh_token");
      }
      if (getCookie("token_expires") != '') {
        $rootScope.tokenExpires = getCookie("token_expires");
      }

      if ((!$rootScope.accessToken || $rootScope.accessToken === null) && toState.name !== 'login') {
        event.preventDefault();
        $state.go('login');
      } else {
        if ($rootScope.accessToken && $rootScope.accessToken !== null && ($rootScope.authenticatedUser === undefined || $rootScope.authenticatedUser === null)) {
          loginService.getUsersDetail(function (value_user, httpHeaders) {
            $rootScope.authenticatedUser = value_user.data;
            refrescarTokenUser.launch_sync();
            //Falta echar proceso de cookies
            if (toState.name === 'login') {
              $state.go('app');
            }
          }, function (httpResponse) {
            console.log(httpResponse);
            loginService.logOut(function () {
            }, function () {
            });
            $state.go('login');
          });

        }
      }
    }
  );
  comerciantesApp.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
      // permanentErrors: [404, 500, 501],
      // maxChunkRetries: 1,
      // chunkRetryInterval: 5000,
      // simultaneousUploads: 4,
      singleFile: true
    };
    flowFactoryProvider.on('catchAll', function (event) {
      // console.log('catchAll', arguments);
    });
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
  }]);
  //$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, options){
  //})
  // $rootScope.$on('$viewContentLoaded', function () {
  //     $templateCache.removeAll();
  // });
  //$http.defaults.headers.common.Authorization = 'Bearer e175fd5e-f328-424d-b54c-324a7b253d47';
}]);
//se utiliza el metodo getcookie en la parte de arriba
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
}
