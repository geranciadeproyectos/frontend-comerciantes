'use strict';

/**
 * @ngdoc function
 * @name yeomanGeneratedProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanGeneratedProjectApp
 */
angular.module('yeomanGeneratedProjectApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
