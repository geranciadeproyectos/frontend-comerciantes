'use strict';
comerciantesApp = angular.module('bcscApp');

comerciantesApp.factory("factoryConfiguracion",
  [
    function () {
      this.configuracion;
      this.setConfiguracion = function (configuracion) {
        this.configuracion = configuracion;
      };
      this.getConfiguracion = function () {
        return this.configuracion;
      };
      return this;
    }]);
