'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.service("serviceUsuario",
  ["$http", '$q', 'APP_CONFIG',
    function ($http, $q, config) {


      this.getAll = function () {
        // console.log('Obteniendo todas las oficinas del servidor');
        return $http({
          method: 'GET',
          url: config().path.api.empresa.usuario
        });
      };

      this.getAllRoles = function () {
        // console.log('Obteniendo todas las oficinas del servidor');
        return $http({
          method: 'GET',
          url: config().path.api.empresa.rol
        });
      };

      this.getAll = function () {
        // console.log('Obteniendo todas las oficinas del servidor');
        return $http({
          method: 'GET',
          url: config().path.api.empresa.usuario
        });
      };

      this.create = function (cuenta) {
        return $http({
          method: 'POST',
          url: config().path.api.empresa.usuario,
          data: cuenta
        });
      };

      this.update = function (cuenta) {

        return $http({
          method: 'PUT',
          url: config().path.api.empresa.usuario,
          data: cuenta
        });
      };

      this.delete= function (uuidCuenta) {
        return $http({
          method: 'DELETE',
          url: config().path.api.empresa.usuario+"/"+uuidCuenta
        });
      };

    }
  ]);
