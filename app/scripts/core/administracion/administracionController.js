'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.controller('administracionController',
  ["$scope", "$rootScope", "$compile", "$uibModal", "serviceAdministracion", 'mhFactoryLoader', "serviceManageError",
    function ($scope, $rootScope, $compile, $uibModal, $serviceAdministracion, $mhFactoryLoader, $serviceManageError) {
      var nameData = "usuario";
      $scope.attrId = "username";
      var attrFilter = "nombre";
      $scope.currentUsuario = $rootScope.currentUsuario;
      var constraints = {};


      $scope.openModalBackup = function () {
        $scope.modal = $uibModal.open({
          animation: true,
          backdrop: false,
          keyboard: false,
          scope: $scope,
          templateUrl: 'backup.html'
        });
      };

      $scope.openModalRestore = function () {
        $scope.modal = $uibModal.open({
          animation: true,
          backdrop: false,
          keyboard: false,
          scope: $scope,
          templateUrl: 'restore.html'
        });
      };

      $scope.cancelModal = function () {
        $scope.uuidEmpresa = null;
        $scope.newUser = {};
        $scope.editUser = {};
        $scope.editData = {};
        $scope.newUser = {};
        $scope.file = null;
        $scope.imageSrc = null;
        $scope.modal.close();
      };

      $scope.backup = function () {
        $serviceAdministracion.backup();
      }

      $scope.restore = function () {
        if (!angular.isUndefined($scope.archivo)) {
          var file = $scope.archivo;
          // console.log("Imprimiendo informacion del archivo", file, file.type, infoFile);
          $scope.cancelModal();
          $mhFactoryLoader.show("Cargando información de restauración del sistema.");

          $serviceAdministracion.restore(file)
            .success(function () {
              $mhFactoryLoader.hide();
              showAlertSucess('Se cargo exitosamente los datos');
              $scope.archivo = undefined;
            })
            .error(function (error) {
              $mhFactoryLoader.hide();
              $scope.archivo = undefined;
              showAlertError($serviceManageError.processError(error));
            });
        } else {
          showAlertInfo("Debe seleccionar el archivo para realizar la carga de la información");
        }
      }

      $scope.alerts = [];
      function showAlertSucess(message) {
        $scope.alerts.push({
          type: 'success',
          msg: message
        });
      }

      function showAlertInfo(message) {
        $scope.alerts.push({
          type: 'info',
          msg: message
        });
      }

      function showAlertError(message) {
        $scope.alerts.push({
          type: 'danger',
          msg: message
        });
      }

      function showAlertWarning(message) {
        $scope.alerts.push({
          type: 'warning',
          msg: message
        });
      }

      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };

    }]);
