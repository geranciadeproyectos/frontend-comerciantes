comerciantesApp.controller('headerController', [
  '$scope', '$rootScope', '$state', 'loginService', 'serviceEmpresa',
  function ($scope, $rootScope, $state, loginService, serviceEmpresa) {
    $scope.currentUsuario = {};

    $scope.changePassword = function changePassword() {
      $state.go('app.editUser');
    };

    $scope.logOut = function logOut() {
      loginService.logOut(function (value) {
        $rootScope.accessToken = undefined;
        $state.go('login');
      });
    };

    angular.element(document).ready(function () {
      loginService.getUsersDetail(
        function (value_user, httpHeaders) {
          $rootScope.currentUsuario = value_user.data;
          $scope.currentUsuario = $rootScope.currentUsuario;
        }, function (httpResponse) {
          $scope.logOut();
        });
    });
  }]);
