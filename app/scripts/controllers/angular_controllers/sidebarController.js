comerciantesApp.controller(
  'sidebarController', ['$scope', '$rootScope', '$state', 'serviceEmpresa',
    function ($scope, $rootScope, $state, serviceEmpresa) {
      $scope.permissions = {};
      $scope.loadInfo = 0;
      $scope.currentEmpresa = {};
      $scope.currentUsuario = {};
      $scope.tipo = "";

      $scope.validatePermission = function () {
        var permissions = {
          usuarios: false,
          roles: false,
          empresas: {
            show: false,
            tipo: ""
          },
          gestiondedatos: false,
          reporte: {
            crear : true,
            listar : false
          }
        };

        if ($scope.currentUsuario) {
          var rol = $rootScope.currentUsuario.usuario.rol.nombre;
          var tipo = $rootScope.currentEmpresa.tipo.nombre;
          var tipoEmpresaAdministrada = $rootScope.currentEmpresa.tipoEmpresaAdministrado;
          $scope.tipo = tipo;
          //console.log(rol, tipo);
          if (tipo !== "SISTEMA" && rol === 'ADMINISTRADOR PRINCIPAL' || rol === 'ADMINISTRADOR') {
            permissions.usuarios = true;
          } else if (rol === 'ADMINISTRADOR PRINCIPAL' && tipo === "SISTEMA") {
            permissions.gestiondedatos = true;
            permissions.reporte.listar = true;
          }
          if (!validate.isEmpty(tipoEmpresaAdministrada) && (rol === 'ADMINISTRADOR PRINCIPAL' /*|| rol === 'ADMINISTRADOR'*/)) {
            permissions.empresas.show = true;
            permissions.empresas.tipo = tipoEmpresaAdministrada.nombre;
          }
        }
        $scope.permissions = permissions;
        //console.log("Permisos concedidos", $scope.permissions);
      };

      $scope.getLogo = function (id) {
        if (validate.isEmpty(id)){
         return 'images/default.gif';
        }else {
          return serviceEmpresa.getLogo(id);
        }
      };

      $rootScope.$watch('currentUsuario', function (newVal) {
        if (!validate.isEmpty(newVal)) {
          //console.log("Actualizo Usuario", $rootScope.currentUsuario, newVal);
          $scope.currentUsuario = $rootScope.currentUsuario;
          $scope.loadInfo++;
        }
      });

      $rootScope.$watch('currentEmpresa', function (newVal) {
        if (!validate.isEmpty(newVal)) {
          //console.log("Actualizo currentEmpresa", $rootScope.currentEmpresa, newVal);
          $scope.currentEmpresa = $rootScope.currentEmpresa;
          $scope.loadInfo++;
        }

      });

      $scope.$watch('loadInfo', function (val) {
        //console.log($scope.loadInfo);
        if ($scope.loadInfo >= 2) {
          //console.log("Lammando informacion de permisos", $scope.currentUsuario, $scope.currentEmpresa);
          $scope.validatePermission();
          $scope.loadInfo = 0;
        }
      });

      angular.element(document).ready(function () {
        serviceEmpresa.get()
          .success(function (response) {
            serviceEmpresa.getNivelEmpresaAdministrado()
              .success(function (data) {
                $rootScope.currentEmpresa = response;
                $rootScope.currentEmpresa.tipoEmpresaAdministrado = data;
              })
              .error(function (error) {
                //console.log(error);
              });
          })
          .error(function (error) {
            //console.log(error);
          });
      });
    }])
;
