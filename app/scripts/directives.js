/**
 * Created by andiro on 20/06/16.
 */
var misterhouseAngular = angular.module('misterhouseAngular', ['ui.bootstrap'
]);
//--Informacion-Oficinas--//
// misterhouseAngular.directive('misterHouseHandsontable', function ($compile) {
//   console.log('Entrando a la directiva de handsontable');
//   return {
//     restrict: 'EA',
//     link: function (scope, element, attrs, getPlugin) {
//       var data = scope[attrs.source];
//       $(element).handsontable({
//         data: data,
//         colHeaders: function (col) {
//           className: "htCenter";
//           var txt = "<input type='checkbox'  class='checker' " + (isChecked() ? 'checked="checked"' : '') + '>';
//           headers = [txt, 'Codigo', 'Nombre','Considerar',
//             'Factor','Zona', 'Region', 'Cuidad','Categoria',
//             'Numero De Cajacontents','Terminales Finacieras','Porcentaje Clientes','Porcentaje Usuarios','TX Nivel',
//             'Horarios'];
//           return headers[col];
//         },
//         // autoWrapRow: true,
//         // minSpareRows: true,
//         // columnSorting: true,
//         // fillHandle: false,
//         // currentRowClassName: 'currentRow',
//         // renderAllRows: true,
//         stretchH: 'all',
//         className: "htCenter",
//         columns: [
//           {data: "active", type: 'checkbox'},
//           {data: "codigo", type: 'text'},
//           {data: "nombre", type: 'text'},
//           {data: "considerar", type: 'checkbox'},
//           {data: "factor", type: 'numeric'},
//           {data: "zona", type: 'text'},
//           {data: "regional", type: 'text'},
//           {data: "cuidad", type: 'text'},
//           {data: "categoria", type: 'text'},
//           {data: "numeroDeCaja", type: 'numeric'},
//           {data: "terminalesFinancieras", type: 'numeric'},
//           {data: "porcentajeClientes", type: 'numeric'},
//           {data: "porcentajeUsuarios", type: 'numeric'},
//           {data: "txNivel", type: 'text'},
//           {data: "horario", renderer: button, readOnly: true},
//         ],
//
//         contextMenu: {
//           callback: function (key, options) {
//             if (key === 'about') {
//               setTimeout(function () {
//                 // timeout is used to make sure the menu collapsed before alert is shown
//                 alert("This is a context menu with default and custom options mixed");
//               }, 100);
//             }
//           },
//           items: {
//             //"row_above": {"name":"insterta fila "},
//             "row_below": {"name":"Desplazar Celda Hacia Abajo"},
//           }}
//       });
//       // ==============================isChecked==================================//
//
//       $(element).on('mouseup', 'input.checker', function (event) {
//         var current = !$('input.checker').is(':checked'); //returns boolean
//         for (var i = 0, ilen = data.length; i < ilen; i++) {
//           data[i].active = current;
//         }
//         $(element).handsontable('render');
//
//       });
//       function isChecked() {
//         for (var i = 0, ilen = data.length; i < ilen; i++) {
//           if (!data[i].active) {
//             return false;
//           }
//         }
//         return true;
//       }
//       function button(instance, td, row, col, prop, value, cellProperties) {
//         td.innerHTML = "<mister-House-Popups></mister-House-Popups>";
//         $compile(element.contents())(scope);
//       }
//     }
//
//   };
// });
// misterhouseAngular.directive('misterHousePopups', function () {
//   console.log("Entrando al pop pup");
//   return {
//     templateUrl: '../views/Informacion/PopUpsInformacion/pruebas.html'
//   }
// })
//
// misterhouseAngular.directive('misterHousePopUpsCalendarios', function () {
//   return {
//     restrict: 'EA',
//     link: function (scope, element, attrs) {
//       var data = scope[attrs.source];
//       $(element).fullCalendar({
//         data: data,
//         eventOverlap: false,
//         slotEventOverlap: false,
//         lang: 'es', //LENGUAJE
//         header: {
//           left: '',
//           center: '',
//           right: ''
//         },
//         defaultView: 'agendaWeek',  //VISTA PRINCIPAL
//         allDaySlot: false,  //desabilita la barra superior
//         columnFormat: {week: 'dddd'}, // MOSTRAR SOLO LOS NOMBRE DE LAS COLUMNAS
//         dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
//         //==================================================================================================
//         //======================================Config inicial==============================================
//         eventStartEditable: true, //EVENTO DESPUES DE CREADO NO LO DEJA MOVER
//         // selectAuto: false,
//         selectable: true, // Habilita la accion de seleccion de creacion de eventos
//         selectHelper: true, // opcion para mostrar una guia en la accion de seleccion de creacion de evento
//         selectOverlap: false, //bloquea el espacio de un evento apra que no sobreescriban sobre el evento existente
//         //businessHours:true,//habilitar horas de trbajo empresariales
//         //editable: true,// editar un evento para que cresca
//         //eventStartEditable: false, //EVENTO DESPUES DE CREADO NO LO DEJA MOVER
//         //==================================================================================================
//         //======================================insertar evento=============================================
//         select: function (start, end) {
//
//
//           var title = start.format("dddd-DD-MM-YYYY-h:mm") + end.format("dddd-DD-MM-YYYY-h:mm");
//           var eventData;
//           if (start.format("dddd-DD-MM-YYYY") == end.format("dddd-DD-MM-YYYY")) {
//             console.log("entre a if")
//             eventData =
//             {
//               start: start,
//               end: end
//
//             };
//
//              data = {
//               dia: start.format("dddd"),
//               horaInicio: start.format("h:mm"),
//               horafin: end.format("h:mm")
//             };
//             console.log(data);
//
//             // var evento =[];
//             // evento.push( start.format("dddd"),start.format("h:mm"),end.format("h:mm"));
//             // console.log(evento);
//             // for (var i=0;i<evento.length;i++){
//             //   console.log(i+evento[i]);
//             // }
//
//             $(element).fullCalendar('renderEvent', eventData, true); // pintar evento en calendario
//           }
//
//           $(element).fullCalendar('unselect');
//         },
//
//         eventRender: function (eventData, element1) {
//           element1.prepend('<div><span class="fc-content" style="float: right;padding-right: 5px;"" >X</span></div>');
//           element1.find(".fc-content").click(function () {
//             console.log("Se da click sobre el boton de cerrar" + eventData._id);
//             $(element).fullCalendar('removeEvents', eventData._id);
//           });
//         },
//         // eventRender: function(eventData,element1){
//         //   $("#limpiar").click(function() {
//         //     console.log("Se da click sobre el boton de cerrar"+eventData._id);
//         //     $(element).fullCalendar('removeEvents', eventData._id);
//         //   });
//         // },
//       })
//     }
//   }
// })
// // misterhouseAngular.directive('misterHousePopupsOficina', function () {
// //   console.log("Entrando al pop pup oficina");
// //   return {
// //     templateUrl: '../views/Informacion/PopUpsInformacion/PopUpsOficiona.html'
// //   }
// // })
