'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.controller('empresaController',
  ["$scope", "$compile", "$uibModal", "serviceEmpresa", "factoryData", "$state", 'mhFactoryLoader', "serviceManageError", 'fileReader',
    function ($scope, $compile, $uibModal, $serviceEmpresa, $factoryData, $state, $mhFactoryLoader, $serviceManageError, fileReader) {
      var nameData = "empresa";
      $scope.attrId = "nombre";
      var attrFilter = "nombre";
      var constraints = {};


      $factoryData.setDataDefault(nameData, {
        id: "",
        nombre: "",
        nit: "",
        telefono: "",
        usuarios: []
      });
      $scope.page = {
        title: "Información de Empresas",
        buttons: [
          {
            show: 'noExisteFilterValue',
            title: 'Refrescar datos',
            action: 'refresh',
            icon: 'fa-refresh'
          },
          {
            show: 'noExisteFilterValue',
            title: 'Adicionar nuevo Distribuidor',
            action: 'openCrearEmpresa',
            icon: 'fa-plus-circle'
          },
        ],
        modal: {
          create: {
            title: "Crear Empresa",
          },
          edit: {
            title: "Editar Empresa",
          },
          usuarios: {
            title: 'lista de usuarios del distribuidor',
            buttons: [
              {
                show: 'noExisteFilterValue',
                title: 'Adicionar nuevo Usuarios Principal',
                action: 'openCrearUsuarioAdministrador',
                icon: 'fa-plus-circle'
              },
              {
                show: true,
                title: 'Elimiar Usuario Principal',
                action: 'deleteUsuarioPrincipal',
                icon: 'fa-minus-circle'
              }
            ]
          }
        }
      };

      $scope.data = $factoryData.getDataWithoutRemoved(nameData);
      $scope.dataUsuarios = [];
      $scope.statusLoad = 0;
      $scope.filterValue = "";
      $scope.newData = {};
      $scope.newAdmin = {};
      $scope.editAdmin = {};
      $scope.editData = {};

      $scope.validarCrearUsuarioAdministrador = function (index) {
        var empresa = JSON.parse(JSON.stringify($scope.data[index]));
        if (empresa.hasOwnProperty('usuarios') && empresa.usuarios[0] != null) {
          return false;
        }
        return true;
      };

      $scope.getFile = function (file) {
        $scope.file = file;
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.file, $scope)
          .then(function (result) {
            $scope.imageSrc = result;
          });
      };

      $scope.callFunction = function (name, params) {
        if (angular.isFunction($scope[name])) {
          if (validate.isEmpty(params)) {
            return $scope[name]();
          } else {
            return $scope[name](params);
          }
        } else {
          return name;
        }
      };

      $scope.refresh = function () {
        $mhFactoryLoader.show("Refrescando la información de los Distribuidors registrados");
        $scope.getAllData(true);
      };

      $scope.getAllData = function (showMessage) {
        $factoryData.clearData(nameData);
        $serviceEmpresa.getAll()
          .success(function (response) {
            if (!validate.isEmpty(showMessage) && showMessage) {
              showAlertSucess('Se cargaron ' + response.length + ' empresas correctamente');
            }
            $factoryData.setData(nameData, response);
            if (!$scope.noExisteFilterValue()) {
              $scope.data = $factoryData.getDataFilterContainsByAttribute(nameData, attrFilter, $scope.filterValue);
            } else {
              $scope.data = $factoryData.getDataWithoutRemoved(nameData);
            }
            $scope.statusLoad++;
          })
          .error(function (error) {
            showAlertError($serviceManageError.processError(error));
            $scope.statusLoad++;
          });
      };

      $scope.getLogo = function (id) {
        return $serviceEmpresa.getLogo(id);
      };

      $scope.save = function () {
        console.log($scope.newData);
        var constraints = {
          nombre: {
            presence: {message: "^El nombre de la empresa es un valor obligatorio y no puede estra vacio"}
          },
          nit: {
            numericality: {message: "^El NIT debe ser un valor numérico no negativo", greaterThan: 0},
            presence: {message: "^El NIT es un valor obligatorio y no puede estra vacio"},
          },
          telefono: {
            numericality: {
              message: "^El teléfono de la empresa debe ser un valor numérico no negativo ",
              greaterThan: 0
            },
          }
        };
        $scope.errorValidation = $scope.validateData($scope.newData, constraints);
        if ($scope.errorValidation.length === 0) {
          $mhFactoryLoader.show('Realizando la creación de la empresa');
          $serviceEmpresa.create($scope.newData, $scope.file)
            .success(function () {
              $scope.cancelModal();
              $scope.getAllData();
            })
            .error(function (error) {
              $mhFactoryLoader.hide();
              showAlertError($serviceManageError.processError(error));
            });

        } else {
          $scope.openModalValidateError();
        }
      };

      $scope.update = function () {

        var constraints = {
          nombre: {
            presence: {message: "^El nombre de la empresa es un valor obligatorio y no puede estra vacio"}
          },
          nit: {
            numericality: {message: "^El NIT debe ser un valor numérico no negativo", greaterThan: 0},
            presence: {message: "^El NIT es un valor obligatorio y no puede estra vacio"},
          },
          telefono: {
            numericality: {
              message: "^El teléfono de la empresa debe ser un valor numérico no negativo ",
              greaterThan: 0
            },
          }
        };

        $scope.errorValidation = $scope.validateData($scope.editData, constraints);

        if ($scope.errorValidation.length === 0) {
          $mhFactoryLoader.show('Realizando la actualización de la empresa');
          $serviceEmpresa.update($scope.editData, $scope.file)
            .success(function () {
              $scope.cancelModal();
              $scope.getAllData();
            })
            .error(function (error) {
              $mhFactoryLoader.hide();
              showAlertError($serviceManageError.processError(error));
            });

        } else {
          $scope.openModalValidateError();
        }
      }
      ;

      $scope.delete = function (id) {
        $mhFactoryLoader.show('Realizando eliminación de la empresa');
        $serviceEmpresa.delete(id)
          .success(function () {
            $scope.getAllData();
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });
      };

      $scope.saveAdministrador = function () {

        console.log($scope.newAdmin);

        var constraints = {
          "usuario.nombre": {

            presence: {message: "^El nombre de usuario es un valor obligatorio y no puede estra vacio"}
          },
          "usuario.cedula": {
            presence: {message: "^El número de cédula es un valor obligatorio y no puede estra vacio"},
            numericality: {message: "^El número de cédula debe ser un valor númerico no negativo",  greaterThan: 0}
          },
          username: {
            presence: {message: "^la cuenta de usuario es un valor obligatorio y no puede estra vacio"}
          },
          password: {
            presence: {message: "^La contraseña es un valor obligatorio y no puede estra vacio"},
            equality: {
              attribute: "passwordValidate",
              message: "^Las contraseñas no son iguales"
            }
          },
          passwordValidate: {
            presence: {message: "^La confirmación de contraseña es un valor obligatorio y no puede estra vacio"},
          }
        };

        $scope.errorValidation = $scope.validateData($scope.newAdmin, constraints);
        if ($scope.errorValidation.length === 0) {
          $mhFactoryLoader.show('Realizando la creación del administrador principal de la empresa');
          $serviceEmpresa.createAdministrador($scope.newAdmin, $scope.uuidEmpresa)
            .success(function () {
              $scope.cancelModal();
              $scope.getAllData();
            })
            .error(function (error) {
              $mhFactoryLoader.hide();
              showAlertError($serviceManageError.processError(error));
            });

        } else {
          $scope.openModalValidateError();
        }
      };

      $scope.updateAdministrador = function () {
        console.log($scope.uuidEmpresa, $scope.editAdmin);
        var constraints = {
          "usuario.nombre": {

            presence: {message: "^El nombre de usuario es un valor obligatorio y no puede estra vacio"}
          },
          "usuario.cedula": {
            presence: {message: "^El número de cédula es un valor obligatorio y no puede estra vacio"},
            numericality: {message: "^El número de cédula debe ser un valor númerico no negativo", greaterThan: 0}
          },
          username: {
            presence: {message: "^la cuenta de usuario es un valor obligatorio y no puede estra vacio"}
          },
          password: {
            presence: {message: "^La contraseña es un valor obligatorio y no puede estra vacio"},
            equality: {
              attribute: "passwordValidate",
              message: "^Las contraseñas no son iguales"
            }
          },
          passwordValidate: {
            presence: {message: "^La confirmación de contraseña es un valor obligatorio y no puede estra vacio"},
          }
        };

        $scope.errorValidation = $scope.validateData($scope.editAdmin, constraints);
        if ($scope.errorValidation.length === 0) {
          $mhFactoryLoader.show('Realizando la actualización del administrador principal de la empresa');
          $serviceEmpresa.updateAdministrador($scope.editAdmin)
            .success(function () {
              $scope.cancelModal();
              $scope.getAllData();
            })
            .error(function (error) {
              $mhFactoryLoader.hide();
              showAlertError($serviceManageError.processError(error));
            });
        } else {
          $scope.openModalValidateError();
        }
      };

      $scope.deleteAdministrador = function (id) {
        $mhFactoryLoader.show('Realizando eliminación del usuario administrador dela empresa');
        $serviceEmpresa.deleteAdministrador(id)
          .success(function (data) {
            $scope.getAllData();
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });
      };

      $scope.openCrearEmpresa = function () {
        $scope.modal = $uibModal.open({
          animation: true,
          backdrop: false,
          keyboard: false,
          scope: $scope,
          templateUrl: 'crearEmpresa.html'
        });
      };

      $scope.openEditarEmpresa = function (index) {
        $scope.editData = JSON.parse(JSON.stringify($scope.data[index]));
        $scope.imageSrc = $scope.getLogo($scope.editData.uuid);
        $scope.modal = $uibModal.open({
          animation: true,
          backdrop: false,
          keyboard: false,
          scope: $scope,
          templateUrl: 'editarEmpresa.html'
        });
      };

      $scope.openCrearAdministrador = function (index) {
        $scope.uuidEmpresa = $scope.data[index].uuid;
        $scope.modal = $uibModal.open({
          animation: true,
          backdrop: false,
          keyboard: false,
          scope: $scope,
          templateUrl: 'crearAdministrador.html'
        });
      };

      $scope.openEditarAdministrador = function (index) {
        $scope.uuidEmpresa = $scope.data[index].uuid;
        $serviceEmpresa.getAdministrador($scope.uuidEmpresa)
          .success(function (data) {
            $scope.editAdmin = data;
            $scope.editAdmin.passwordValidate = $scope.editAdmin.password;
            $scope.modal = $uibModal.open({
              animation: true,
              backdrop: false,
              keyboard: false,
              scope: $scope,
              templateUrl: 'editarAdministrador.html'
            });
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });
      };

      $scope.cancelModal = function () {
        $scope.uuidEmpresa = null;
        $scope.newAdmin = {};
        $scope.editAdmin = {};
        $scope.editData = {};
        $scope.newData = {};
        $scope.file = null;
        $scope.imageSrc = null;
        $scope.modal.close();
      };


      $scope.openModalValidateError = function () {
        $scope.modalError = $uibModal.open({
          animation: true,
          backdrop: false,
          keyboard: false,
          scope: $scope,
          templateUrl: 'validacionDatos.html'
        });
      };

      $scope.cancelModalError = function () {
        $scope.modalError.close();
      };


      $scope.noExisteCambios = function () {
        var existeCambios = false;
        angular.forEach($factoryData.getData(nameData), function (value) {
          if (!validate.isEmpty(value._status)) {
            existeCambios = true;
          }
        });
        return existeCambios;
      };

//================================operaciones desde memoria======================
      $scope.noExisteFilterValue = function () {
        return (validate.isEmpty($scope.filterValue));
      };

      $scope.filter = function () {
        if (!$scope.noExisteFilterValue()) {
          $scope.data = $factoryData.getDataFilterContainsByAttribute(nameData, attrFilter, $scope.filterValue);
        } else {
          $scope.data = $factoryData.getDataWithoutRemoved(nameData);
        }
      };

      $scope.changeFilter = function () {
        $scope.filter();
      };

      $scope.validateData = function (data, constraints) {

        console.log("validando los datos");
        var errorsValidation = [];
        var error = validate(data, constraints);
        console.log("error : ", error);
        if (!angular.isUndefined(error)) {
          angular.forEach(error, function (value, attr) {
            errorsValidation.push({attr: attr, error: value.join()})
          });
        }
        // console.log("Errores de validacion de datos :", errorsValidation );
        return errorsValidation;
      };


      $scope.alerts = [];
      function showAlertSucess(message) {
        $scope.alerts.push({
          type: 'success',
          msg: message
        });
      }

      function showAlertInfo(message) {
        $scope.alerts.push({
          type: 'info',
          msg: message
        });
      }

      function showAlertError(message) {
        $scope.alerts.push({
          type: 'danger',
          msg: message
        });
      }

      function showAlertWarning(message) {
        $scope.alerts.push({
          type: 'warning',
          msg: message
        });
      }

      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      //====================================== FIN ==============================================//


      $scope.$watch("statusLoad", function () {
        // console.log("Valor del estado de carga de datos", $scope.statusLoad);
        if ($scope.statusLoad >= 1) {
          $mhFactoryLoader.hide();
          $scope.statusLoad = 0;
        }
      });

      angular.element(document).ready(function () {
        if ($factoryData.getDataWithoutRemoved(nameData).length == 0) {
          $mhFactoryLoader.show("Obteniendo información de los Distribuidors registrados");
          $scope.getAllData(true);
        }
      });
    }
  ]);
