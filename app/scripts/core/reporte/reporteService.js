'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.service("serviceReporte",
  ["$http", '$q', 'APP_CONFIG',
    function ($http, $q, config) {

      this.getAll = function () {
        // console.log('Obteniendo todas las oficinas del servidor');
        return $http({
          method: 'GET',
          url: config().path.api.reporte.base
        });
      };

      this.create = function (reporte) {
        return $http({
          method: 'POST',
          url: config().path.api.reporte.base,
          data: reporte
        });
      };

      this.delete= function (uuidReporte) {
        return $http({
          method: 'DELETE',
          url: config().path.api.reporte.base+"/"+uuidReporte
        });
      };
    }
  ]);
