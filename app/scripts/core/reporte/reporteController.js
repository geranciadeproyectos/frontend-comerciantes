'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.controller('reporteController',
  ["$scope", "$rootScope", "$compile", "$uibModal", "serviceReporte", "factoryData", "$state", 'mhFactoryLoader', "serviceManageError", 'fileReader',
    function ($scope, $rootScope, $compile, $uibModal, $serviceReporte, $factoryData, $state, $mhFactoryLoader, $serviceManageError, fileReader) {
      $scope.currentUsuario = $rootScope.currentUsuario;
      $scope.data = [];
      $scope.reporte = {};

      /**
       * Metodo que retorna los Distribuidors registrados en el api
       */
      $scope.getAllData = function (showMessage) {
        $mhFactoryLoader.show('Cargando reportes registrados por los usuarios del sistema');
        $serviceReporte.getAll()
          .success(function (response) {
            $scope.data = response;
            $mhFactoryLoader.hide();
          })
          .error(function (error) {
            showAlertError($serviceManageError.processError(error));
            $mhFactoryLoader.hide();
          });
      };

      $scope.save = function () {
        console.log($scope.reporte);
        $mhFactoryLoader.show('Realizando la creación del reporte');
        $serviceReporte.create($scope.reporte)
          .success(function () {
            showAlertInfo('Se creo el reporte correctamente');
            $scope.reporte = {};
            $mhFactoryLoader.hide();
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });
      };

      $scope.delete = function (id) {
        console.log(id);
        $mhFactoryLoader.show('Realizando eliminación del Reporte');
        $serviceReporte.delete(id)
          .success(function () {
            showAlertSucess("Se elimino el reporte correctamente");
            $scope.getAllData();
          })
          .error(function (error) {
            $mhFactoryLoader.hide();
            showAlertError($serviceManageError.processError(error));
          });
      };





      $scope.alerts = [];
      function showAlertSucess(message) {
        $scope.alerts.push({
          type: 'success',
          msg: message
        });
      }

      function showAlertInfo(message) {
        $scope.alerts.push({
          type: 'info',
          msg: message
        });
      }

      function showAlertError(message) {
        $scope.alerts.push({
          type: 'danger',
          msg: message
        });
      }

      function showAlertWarning(message) {
        $scope.alerts.push({
          type: 'warning',
          msg: message
        });
      }

      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };


      angular.element(document).ready(function () {
        if ($state.current.name === "app.reportes") {
          $mhFactoryLoader.show("Obteniendo información de los reportes registrados");
          $scope.getAllData(true);
        }
      });
    }]);
