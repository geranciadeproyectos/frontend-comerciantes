'use strict';
comerciantesApp = angular.module('bcscApp');
comerciantesApp.service("serviceEmpresa",
  ["$http", '$q', 'APP_CONFIG',
    function ($http, $q, config) {
      this.get = function () {
        // console.log('Obteniendo todas las oficinas del servidor');
        return $http({
          method: 'GET',
          url: config().path.api.empresa.base
        });
      };

      this.getAll = function () {
        // console.log('Obteniendo todas las oficinas del servidor');
        return $http({
          method: 'GET',
          url: config().path.api.empresa.administrada
        });
      };

      this.getEmpresasAdministradas = function (id) {
        var httpUrl = config().path.api.empresa.administrada;
        return $http({
          method: 'GET',
          url: httpUrl
        });
      };

      this.create = function (data, file) {
        var formData = new FormData();
        formData.append("file", validate.isEmpty(file)?null:file);
        formData.append('empresa', new Blob([JSON.stringify(data)], {type: "application/json"}));
        return $http({
          method: 'POST',
          headers: {
            "Content-Type": undefined
          },
          url: config().path.api.empresa.base,
          data: formData
        });
      };

      this.update = function (data, file) {
        var formData = new FormData();
        formData.append("file", validate.isEmpty(file)?null:file);
        formData.append('empresa', new Blob([JSON.stringify(data)], {type: "application/json"}));
        return $http({
          method: 'PUT',
          headers: {
            "Content-Type": undefined
          },
          url: config().path.api.empresa.base,
          data: formData
        });
      };

      this.delete = function (id) {
        return $http({
          method: 'DELETE',
          url: config().path.api.empresa.base+"/"+id
        });
      };

      this.getLogo = function (id){
        return config().path.api.empresa.logo.replace("{id}", id);

      };

      this.getAdministrador = function (id) {
        // console.log('Obteniendo todas las oficinas del servidor');
        return $http({
          method: 'GET',
          url: config().path.api.empresa.administrador,
          params : {uuid: id}
        });
      };

      this.createAdministrador = function (cuenta, uuidEmpresa) {
        return $http({
          method: 'POST',
          url: config().path.api.empresa.administrador,
          data: {
            cuenta:cuenta,
            uuid: uuidEmpresa
          }
        });
      };

      this.updateAdministrador = function (cuenta) {

        return $http({
          method: 'PUT',
          url: config().path.api.empresa.administrador,
          data: cuenta
        });
      };

      this.deleteAdministrador = function (uuidCuenta) {
        return $http({
          method: 'DELETE',
          url: config().path.api.empresa.administrador+"/"+uuidCuenta
        });
      };

      this.getNivelEmpresaAdministrado = function (id) {
        var httpUrl = config().path.api.empresa.tipoEmpresa;
        return $http({
          method: 'GET',
          url: httpUrl
        });
      };
    }
  ]);
