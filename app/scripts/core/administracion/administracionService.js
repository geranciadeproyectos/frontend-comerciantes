'use strict';
comerciantesApp = angular.module('bcscApp');

comerciantesApp.service("serviceAdministracion",
  ["$http", '$q', 'APP_CONFIG',
    function ($http, $q, config) {
      this.backup = function () {
        window.location.href = config().path.api.administracion.backup
      };

      this.restore = function (file) {
        var fd = new FormData();
        fd.append('file', file);
        return $http.post(config().path.api.administracion.restore, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        });
      };
    }]);
