comerciantesApp.factory('authRequestInterceptor', function ($rootScope) {
  return {
    request: function (config) {
  //      config.headers.Authorization = 'Basic SEFOT0lULUFQUDoxMjM0NTY=';
  //     console.log($rootScope.accessToken);
      if (config.headers.Authorization == undefined) {
        config.headers = config.headers || {};
        if ( $rootScope.accessToken !== undefined || $rootScope.accessToken !== null ) {
          config.headers.Authorization = 'Bearer ' + $rootScope.accessToken;
        }
      }
      // console.log(config);
      return config;
    }
  }
});
