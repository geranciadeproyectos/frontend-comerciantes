
comerciantesApp.factory('authResponseInterceptor', function ($rootScope, $q, $location) {
  return {
    response: function (response) {
      $('#mantoBlocker').css('display','none');
      return response;
    },
    responseError: function (rejection) {
        if(rejection.status === 401) {
            if (getCookie("access_token") != '')
            {
    					$rootScope.accessToken = null;
    					document.cookie="access_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
            }
            if (getCookie("refresh_token") != '')
            {
    					$rootScope.refreshToken = null;
    					document.cookie="refresh_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";

            }
            if (getCookie("token_expires") != '') {
    					$rootScope.tokenExpires = null;
              document.cookie="token_expires=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
            }
    				if($rootScope.authenticatedUser !== null || $rootScope.authenticatedUser !== undefined)
    				{
    					$rootScope.authenticatedUser = null;
    				}
            $location.path('/login');
        }
        if(rejection.status === 0)
        {
          //message.show('error', "No hubo respuesta del servidor revise su conección a internet o intente de nuevo más tarde");
          //console.log('no internet no answer');
        }
        return $q.reject(rejection);
    }
  }

  function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i=0; i<ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1);
          if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
      }
      return "";
  }
});
